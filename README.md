# Houston Kubernetes Meetup Nov 2019

Presentation slides: https://docs.google.com/presentation/d/1kXZhaY3TSwWtFHaJwnn7x6sFGQCsWXaWnFVrOUBhRtI/edit?usp=sharing

# Overview:
> Kubernetes is enabling startups and enterprises to manage workloads of increasing scale. Many organizations are deploying custom code to their clusters. Often overlooked, there's a wide variety of open source applications that can replace traditional SAAS offerings, bringing with it improvements in data ownership and compliance.

> This presentation demonstrates some offerings which could bring some software offerings on premise with ease.


## Note, for brevity of presentation:
- default databases utilized (should investigate managed databases with backups and properly configured encryption)
- DNS + HTTPS is not configured. Let's Encrypt is a good starting point for this

## Requirements
- install gcloud cli or use cloud shell
- Setup GKE Cluster
- Install Helm (locally if needed, see getting started)

## Spin up a cluster [link](https://cloud.google.com/kubernetes-engine/docs/quickstart)
```shell
gcloud container clusters create hou-k8-demo \
    --zone us-central1-a --num-nodes 8

# or

gcloud beta container --project "hipspec-demos" clusters create "hou-k8-demo2" --zone "us-central1-a" --no-enable-basic-auth --cluster-version "1.13.11-gke.14" --machine-type "n1-standard-2" --image-type "COS" --disk-type "pd-ssd" --disk-size "50" --metadata disable-legacy-endpoints=true --preemptible --num-nodes "6" --enable-cloud-logging --enable-cloud-monitoring --enable-ip-alias --network "projects/hipspec-demos/global/networks/default" --subnetwork "projects/hipspec-demos/regions/us-central1/subnetworks/default" --default-max-pods-per-node "110" --enable-autoscaling --min-nodes "4" --max-nodes "10" --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair
```

```
gcloud container clusters get-credentials hou-k8-demo --zone us-central1-a --project hipspec-demos
``

## Install Helm [link](https://helm.sh/docs/intro/install/)
```shell
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh

chmod 700 get_helm.sh

./get_helm.sh

helm repo add stable https://kubernetes-charts.storage.googleapis.com/
```

## Keycloak [link](https://hub.helm.sh/charts/codecentric/keycloak)

```
cat keycloak.yaml
helm repo add codecentric https://codecentric.github.io/helm-charts
helm install keycloak -f keycloak.yaml codecentric/keycloak
```

```
export POD_NAME=$(kubectl get pods --namespace default -l app.kubernetes.io/instance=keycloak -o jsonpath="{.items[0].metadata.name}")
  echo "Visit http://127.0.0.1:8080 to use Keycloak"
  kubectl port-forward --namespace default $POD_NAME 8080
```

## Metabase [link](https://github.com/helm/charts/tree/master/stable/metabase)
```
helm install metabase stable/metabase --set image.tag=0.33.5

export POD_NAME=$(kubectl get pods --namespace default -l "app=metabase,release=metabase" -o jsonpath="{.items[0].metadata.name}")
  echo "Visit http://127.0.0.1:8080 to use your application"
  kubectl port-forward --namespace default $POD_NAME 8080:3000

```

## Postgres [link](https://github.com/helm/charts/tree/master/stable/postgresql)

```
helm install my_postgres stable/postgresql
```

## Prisma [link](https://github.com/helm/charts/tree/master/stable/prisma)
```
helm install prisma stable/prisma
```


## Sentry [link](https://github.com/helm/charts/tree/master/stable/sentry)

```
helm install sentry --wait stable/sentry
```

## Weaveworks Scope [link](https://github.com/helm/charts/tree/master/stable/weave-scope)

```
helm install scope stable/weave-scope

export POD_NAME=$(kubectl get pods --namespace default -l app.kubernetes.io/instance=scope -o jsonpath="{.items[0].metadata.name}")
  echo "Visit http://127.0.0.1:8080 to use Keycloak"
  kubectl port-forward --namespace default $POD_NAME 8080

```

## Rocketchat [link](https://github.com/helm/charts/tree/master/stable/rocketchat)

> Note, would probably pick mattermost over this for ability to use managed SQL in google cloud, encrypted at rest, etc.
```
$ helm install rocketchat stable/rocketchat --set mongodb.mongodbPassword=$(echo -n $(openssl rand -base64 32)),mongodb.mongodbRootPassword=$(echo -n $(openssl rand -base64 32))

```

## Gitlab [link](https://hub.helm.sh/charts/gitlab/gitlab)

```
helm repo add gitlab https://charts.gitlab.io/
helm install gitlab -f gitlab.yaml gitlab/gitlab
```

```
POD_NAME=$(kubectl get pods --namespace default -l app.kubernetes.io/instance=gitlab -o jsonpath="{.items[0].metadata.name}")
  echo "Visit http://127.0.0.1:8080 to use Gitlab"
  kubectl port-forward --namespace default $POD_NAME 8080
```

## Ghost [link](https://hub.helm.sh/charts/bitnami/ghost)

```
helm repo add bitnami https://charts.bitnami.com
helm install ghost -f ghost.yaml bitnami/ghost

kubectl get svc --namespace default -w ghost

export APP_HOST=$(kubectl get svc --namespace default ghost --template "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}")

export APP_PASSWORD=$(kubectl get secret --namespace default ghost -o jsonpath="{.data.ghost-password}" | base64 --decode)

export APP_DATABASE_PASSWORD=$(kubectl get secret --namespace default ghost-mariadb -o jsonpath="{.data.mariadb-password}" | base64 --decode)

```

## Kong API mgmt [link](https://hub.helm.sh/charts/stable/kong)
```
helm install kong -f kong.yaml stable/kong
```


## Dokuwiki [link](https://hub.helm.sh/charts/bitnami/dokuwiki)

```
helm repo add bitnami https://charts.bitnami.com
helm install dokuwiki -f dokuwiki.yaml bitnami/dokuwiki
```

## Grafana [link](https://hub.helm.sh/charts/bitnami/grafana)
```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install --name my-release bitnami/grafana
```

## Mattermost [link](https://hub.helm.sh/charts/mattermost/mattermost-team-edition)
```
helm repo add mattermost https://helm.mattermost.com
$ helm install mattermost/mattermost-team-edition \
  --set mysql.mysqlUser=sampleHoustonUser \
  --set mysql.mysqlPassword=sampleHoustonPassword \
```


## Argo [link](https://hub.helm.sh/charts/argo/argo)
> also [demo instructions](https://github.com/argoproj/argo/blob/master/demo.md)
```
# on local machine
brew install argoproj/tap/argo



helm repo add argo https://argoproj.github.io/argo-helm
helm install argo argo/argo

NOTES:
1. Get Argo UI external IP/domain by running:

kubectl --namespace default get services -o wide | grep argo-

kubectl port-forward deployment/argo-ui 8001:8001

# Needed for GKE

kubectl create clusterrolebinding argo-cluster-admin-binding --clusterrole=cluster-admin --user=mason@hipspec.com

kubectl create rolebinding default-admin --clusterrole=admin --serviceaccount=default:default

argo submit --watch https://raw.githubusercontent.com/argoproj/argo/master/examples/hello-world.yaml
argo submit --watch https://raw.githubusercontent.com/argoproj/argo/master/examples/coinflip.yaml
argo submit --watch https://raw.githubusercontent.com/argoproj/argo/master/examples/loops-maps.yaml
argo list
argo get xxx-workflow-name-xxx
argo logs xxx-pod-name-xxx #from get command above


```